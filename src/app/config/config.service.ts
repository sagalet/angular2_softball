import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Item {
  Path: string;
}

export interface Cate {
  Index: Item;
  Pic: Item;
  Js: Item;
}

export interface ServiceConfig {
  Port: string;
  Path: string;
  Category: Cate;
}

@Injectable()
export class ConfigService {

  ConfigUrl = "http://localhost/json/";
  ProtoUrl = "http://localhost/proto/";

  constructor(private http: HttpClient) { }

  getConfig() {
    console.log("try get config");
    return this.http.get<ServiceConfig>(this.ConfigUrl);
  }

  getProto() {
    console.log("try get proto");
    return this.http.get(this.ProtoUrl, {responseType: 'arraybuffer'});
  }
}
