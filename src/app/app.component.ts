import { Component } from '@angular/core';
import { ConfigService, ServiceConfig } from './config/config.service';
import { Pack1 } from '../../build/message_pb';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ConfigService],
})

export class AppComponent {
  title = 'my first angular app';
  tableName = 'this is a table';
  tableContent = 'this is content';
  index = 0;
  titleList = ["my first app", "my second app", "my third app"];
  inputValue = "";
  pwdValue = "";
  pack: Pack1;

  showTitle = true;
  showTitleLabel = ["show title", "hide title"];
  titleLabel = this.showTitleLabel[1];

  config: ServiceConfig;

  constructor(private configService: ConfigService) {
    console.log("constructor");
  }

  getDate() {
    console.log("now check date");
    return (new Date());
  }
  
  doSomething($event) {
    console.log("now try reflash", $event);
    this.inputValue = this.titleList[this.index++];
    if(this.index >= this.titleList.length) {
      this.index = 0;
    }
  }

  changeTitleStatus($event) {
    this.showTitle = !this.showTitle;
    console.log("this.showTitle", this.showTitle);
    if(this.showTitle) {
      this.titleLabel = this.showTitleLabel[1];
    } else {
      this.titleLabel = this.showTitleLabel[0];
    }
  }

  showConfig($event) {
    this.configService.getConfig()
      .subscribe(
        (data: ServiceConfig) => {
          this.config = data;
          console.log("get json", this.config);
        }
      )
  }

  showProto($event) {
    console.log("try get proto");
    this.configService.getProto()
      .subscribe(
        data => {
          this.pack = Pack1.deserializeBinary(new Uint8Array(data));
          console.log("pack1=", this.pack.getDataList());
        }
      )
  }
}
