// package: content1
// file: message.proto

import * as jspb from "google-protobuf";

export class Data1 extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getId(): number;
  setId(value: number): void;

  getMsg(): string;
  setMsg(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Data1.AsObject;
  static toObject(includeInstance: boolean, msg: Data1): Data1.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Data1, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Data1;
  static deserializeBinaryFromReader(message: Data1, reader: jspb.BinaryReader): Data1;
}

export namespace Data1 {
  export type AsObject = {
    name: string,
    id: number,
    msg: string,
  }
}

export class Pack1 extends jspb.Message {
  clearDataList(): void;
  getDataList(): Array<Data1>;
  setDataList(value: Array<Data1>): void;
  addData(value?: Data1, index?: number): Data1;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Pack1.AsObject;
  static toObject(includeInstance: boolean, msg: Pack1): Pack1.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Pack1, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Pack1;
  static deserializeBinaryFromReader(message: Pack1, reader: jspb.BinaryReader): Pack1;
}

export namespace Pack1 {
  export type AsObject = {
    dataList: Array<Data1.AsObject>,
  }
}

